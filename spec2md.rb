#!/usr/bin/env ruby

require 'yaml'

DEFAULT = {
  map: "5×5 的空白方格。\n",
  length: 'length=|Δx|+|Δy|',
  setup: "每位玩家秘密选择位置。\n",
  win: "当仅有一位玩家没有结束游戏，该玩家获胜。\n"
}.freeze

spec = YAML.load(STDIN.read)

DEFAULT.each do |key, value|
  key = key.to_s
  spec[key] = value if spec[key].nil?
end

raise 'Need name' if spec['name'].nil?
raise 'Need round' if spec['round'].nil?

puts "# #{spec['name']}"

if spec['uri'] || spec['author']
  author = "@#{spec['author']}" if spec['author']
  author = "[#{author}](#{spec['author_uri']})" if spec['author_uri'] && author
  print '来自'
  print " #{spec['uri']}" if spec['uri']
  print " #{author}" if author
  puts
  puts
end

if spec['background']
  puts '## 背景故事'
  puts spec['background'].gsub("\n", "\n\n").chomp
  puts
end

puts '## 暗室地图'
puts spec['map']
puts

puts '## 距离计算'
puts spec['length']
puts

puts '## 初始设置'
puts spec['setup']
puts

puts '## 玩家行动'
puts <<DES
所有玩家开始时处于阶段 0，依次进入下一个大于当前阶段数中阶段数最小的阶段。

每个阶段中玩家可以选择当前阶段列表中任何一条执行。

当玩家完成阶段数最大的阶段，轮到下一位玩家行动。

DES
spec['round'].each do |n, item|
  puts "### 阶段 #{n}"
  item.each do |action|
    if action.nil?
      puts '* None'
      next
    end
    puts "* #{action['name']}"
    puts action['description'].gsub("\n", "\n\n").chomp.gsub!(/^/, '> ')
  end
  puts
end

puts '## 胜利条件'
puts spec['win']
